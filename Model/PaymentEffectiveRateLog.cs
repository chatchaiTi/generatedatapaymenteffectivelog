﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateDataPaymentEffectiveLog.Model
{
    public class PaymentEffectiveRateLog
    {
        public int PaymentEffectiveRateLogID { get; set; }
        public string ContractNo { get; set; }
        public string SlipNo { get; set; }
        public DateTime SlipDate { get; set; }
        public int EffectiveDay { get; set; }
        public decimal CalculateInterestAmnt { get; set; }
        public decimal BringForwardPrincipalAmnt { get; set; }
        public decimal BringForwardInterestAmnt { get; set; }
        public decimal PayInterestAmnt { get; set; }
        public decimal PayOldInterestAmnt { get; set; }
        public decimal PayNewInterestAmnt { get; set; }
        public decimal PayPrincipalAmnt { get; set; }
        public decimal RemainPrincipalAmnt { get; set; }
        public decimal UnpaidInterestAmnt { get; set; }
        public decimal NetAmnt { get; set; }
        public bool IsCancel { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime? LastCalculateInterestDate { get; set; }
        public int Period { get; set; }
        public DateTime? ContractDueDate { get; set; }
        public decimal InstallmentAmnt { get; set; }
        public decimal PayInstallmentAmnt { get; set; }
        public decimal OverPayInstallmentAmnt { get; set; }
        public decimal RemainInstallmentAmnt { get; set; }
        public bool IsComplete { get; set; }
    }
}
