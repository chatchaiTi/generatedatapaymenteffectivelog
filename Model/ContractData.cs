﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateDataPaymentEffectiveLog.Model
{
    public class ContractData
    {
        public string ContractNo { get; set; }
        public DateTime ContractDate { get; set; }
        public string PayDay { get; set; }
        public DateTime FirstPeriodDate { get; set; }
        public decimal SyMthAmt { get; set; }
        public decimal SyCost { get; set; }
        public decimal SyPkAmt { get; set; }
        public decimal PrincipalAmnt { get { return SyCost + SyPkAmt; } }
        public decimal EirRate { get; set; }
        public int SyMonth { get; set; }
        public DateTime? CloseDate { get; set; }
    }
}
