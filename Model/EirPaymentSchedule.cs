﻿using System;

namespace GenerateDataPaymentEffectiveLog.Model
{
    public class EirPaymentSchedule
    {
        public string ContractNo { get; set; }
        public int PeriodNo { get; set; }
        public DateTime DueDate { get; set; }
        public decimal InstallmentAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal BalanceAmount { get; set; }
        public DateTime? SlipDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
