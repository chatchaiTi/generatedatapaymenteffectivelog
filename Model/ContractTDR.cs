﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateDataPaymentEffectiveLog.Model
{
    public class ContractTDR
    {
        public int ContractTDRID { get; set; }
        public string ContractNo { get; set; }
        public decimal RemainPrincipalAmnt { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }

        public int ContractTDRPaymentTermID { get; set; }
        public int PeroidNo { get; set; }
        public DateTime PeroidDate { get; set; }
        public decimal Installment { get; set; }
    }
}