﻿using GenerateDataPaymentEffectiveLog.Hepler;
using GenerateDataPaymentEffectiveLog.Manager;
using System;
using System.Diagnostics;
using System.Threading;

namespace GenerateDataPaymentEffectiveLog
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("WARNING !! Please check your conection server !!");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(">>> Connect " + GlobalVar.ServerConectionString.Split(';')[0] + " <<< ");
            Console.WriteLine();
            Console.ResetColor();
            Console.WriteLine("When you want to generate every contract ___ press word 'All'");
            Console.Write("Input ContractNo : ");

            string contractNO = Console.ReadLine();
            if (string.IsNullOrEmpty(contractNO))
            {
                End();
                return;
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            PaymentEffectiveLogManager paymentEffectiveLogManager = new PaymentEffectiveLogManager(contractNO);
            paymentEffectiveLogManager.PrepareData();
            paymentEffectiveLogManager.Save();

            stopwatch.Stop();

            Console.WriteLine();
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);
            Console.WriteLine("Time elapsed: {0:hh\\:mm\\:ss}", stopwatch.Elapsed);

            End();
        }

        private static void End()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine("End Task in 10s");
            Thread.Sleep(10);
            Reader.ReadLine(10 * 1000).Trim();
        }
    }
}
