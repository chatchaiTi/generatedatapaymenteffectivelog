﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GenerateDataPaymentEffectiveLog.Hepler
{
    public class QuerySource
    {
        public string FieldName { get; set; }
        public object Value { get; set; }
    }

    public class StringFormatHelper
    {
        public static string GetSingleQuote<T>(T text)
        {
            return string.Format("'{0}'", text.ToString());
        }

        public static string GetShortEnDateFormat(DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));
        }

        public static string GetShortEnDateAndTimeFormat(DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
        }
    }

    public class ReaderHepler
    {
        public static T GetValueFromReader<T>(MySqlDataReader reader, string name)
        {
            object value = reader[name];
            Type t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (t.IsEnum)
            {
                try
                {
                    return value == null || DBNull.Value.Equals(value) || string.IsNullOrEmpty(value.ToString())
                         ? default(T)
                         : (T)Enum.Parse(t, value.ToString());
                }
                catch (Exception ex)
                {
                    return default(T);
                }
            }

            if (typeof(T) == typeof(Boolean))
                return value == null || DBNull.Value.Equals(value)
                    ? default(T)
                    : (T)Convert.ChangeType(Convert.ToInt32(value), t);

            if (typeof(T) == typeof(DateTime?))
                return value == null || DBNull.Value.Equals(value)
                    ? default(T)
                    : (T)Convert.ChangeType(value, t);

            return value == null || DBNull.Value.Equals(value)
                    ? default(T)
                    : (T)Convert.ChangeType(value, t);
        }

        public static T GetValueFromReader<T>(SqlDataReader reader, string name)
        {
            object value = reader[name];
            Type t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (typeof(T) == typeof(Boolean))
                return value == null || DBNull.Value.Equals(value)
                    ? default(T)
                    : (T)Convert.ChangeType(Convert.ToInt32(value), t);

            if (typeof(T) == typeof(DateTime?))
                return value == null || DBNull.Value.Equals(value)
                    ? default(T)
                    : (T)Convert.ChangeType(value, t);

            return value == null || DBNull.Value.Equals(value)
                    ? default(T)
                    : (T)Convert.ChangeType(value, t);
        }
    }

    public class Reader
    {
        private static Thread inputThread;
        private static AutoResetEvent getInput, gotInput;
        private static string input;

        static Reader()
        {
            getInput = new AutoResetEvent(false);
            gotInput = new AutoResetEvent(false);
            inputThread = new Thread(reader);
            inputThread.IsBackground = true;
            inputThread.Start();
        }

        private static void reader()
        {
            while (true)
            {
                getInput.WaitOne();
                input = Console.ReadLine();
                gotInput.Set();
            }
        }

        public static string ReadLine(int timeOutMillisecs)
        {
            getInput.Set();
            bool success = gotInput.WaitOne(timeOutMillisecs);
            if (success)
                return input;

            return string.Empty;
        }
    }
}
