﻿using GenerateDataPaymentEffectiveLog.Hepler;
using GenerateDataPaymentEffectiveLog.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace GenerateDataPaymentEffectiveLog.Manager
{
    public class PaymentEffectiveLogManager
    {
        private string contractNO;
        private List<PaymentEffectiveRateLog> paymentDatas;

        public PaymentEffectiveLogManager(string contractNO)
        {
            this.contractNO = contractNO.Trim();
            this.paymentDatas = new List<PaymentEffectiveRateLog>();
        }

        #region Public Method
        public void PrepareData()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(nameof(PrepareData));

            List<string> contractNOs = new List<string>();
            if (IsAll())
                contractNOs = GetAllContractNoPaymentEffectiveLog();
            else
                contractNOs.Add(contractNO);

            List<ContractData> contractDatas = GetContractDatas();
            List<EirPaymentSchedule> eirPaymentSchedules = GetPeriodNonPayments();
            List<EirPaymentSchedule> eirPaymentAlls = GetAllPaymentSchedule();
            List<PaymentEffectiveRateLog> lastPayments = GetLastPaymentAllContract();
            List<ContractTDR> contractTDRs = GetDataContractTDR();

            int loop = 0;
            foreach (string contractNo in contractNOs)
            {
                ContractData contractData = contractDatas.Find(a => a.ContractNo == contractNo);
                if (contractData is null)
                    continue;

                if (contractData.CloseDate.HasValue) // ปิดบัญชี
                    continue;

                EirPaymentSchedule eirPaymentSchedule = eirPaymentSchedules.Find(a => a.ContractNo == contractNo);
                //if (eirPaymentSchedule is null) // ปิดบัญชี
                //    continue;

                PaymentEffectiveRateLog lastPayment = lastPayments.Find(a => a.ContractNo == contractNo);

                for (int period = 1; period <= contractData.SyMonth; period++)
                {
                    ContractTDR contractTDR = contractTDRs.Find(a => a.ContractNo == contractNo
                                            && a.PeroidNo == period
                                            );

                    EirPaymentSchedule eirPayment = eirPaymentAlls.Find(a => a.ContractNo == contractNo && a.PeriodNo == period);

                    decimal installmentAmnt = eirPaymentSchedule is null ? contractData.SyMthAmt : eirPaymentSchedule.InstallmentAmount;
                    if (contractTDR != null)
                        installmentAmnt = contractTDR.Installment;

                    if (eirPaymentSchedule is null) // สัญญาที่ยังไม่มีการจ่ายตังค์
                    {
                        List<PaymentEffectiveRateLog> paymentCalculates = GetCalculates(contractNo, contractData, contractTDR, period);
                        paymentDatas.AddRange(paymentCalculates);
                        Console.WriteLine(nameof(PrepareData) + " | " + (period) + " | " + contractNo);
                        continue;
                    }

                    if (period < eirPaymentSchedule.PeriodNo)
                    {
                        PaymentEffectiveRateLog paymentLastRow = new PaymentEffectiveRateLog();
                        paymentLastRow.ContractNo = contractNo;
                        paymentLastRow.SlipNo = string.Empty;
                        paymentLastRow.SlipDate = eirPayment.SlipDate.Value;
                        paymentLastRow.LastCalculateInterestDate = new DateTime?();
                        paymentLastRow.Period = period;
                        paymentLastRow.ContractDueDate = eirPayment.DueDate;
                        paymentLastRow.EffectiveDay = 0;
                        paymentLastRow.InstallmentAmnt = installmentAmnt;
                        paymentLastRow.NetAmnt = 0;
                        paymentLastRow.PayInstallmentAmnt = 0;
                        paymentLastRow.OverPayInstallmentAmnt = 0;
                        paymentLastRow.CalculateInterestAmnt = 0;
                        paymentLastRow.PayInterestAmnt = 0;
                        paymentLastRow.PayOldInterestAmnt = 0;
                        paymentLastRow.PayNewInterestAmnt = 0;
                        paymentLastRow.PayPrincipalAmnt = 0;
                        paymentLastRow.RemainPrincipalAmnt = 0;
                        paymentLastRow.UnpaidInterestAmnt = 0;
                        paymentLastRow.RemainInstallmentAmnt = 0;
                        paymentLastRow.IsComplete = true;
                        paymentLastRow.IsCancel = true;
                        paymentLastRow.Remark = string.Empty;

                        paymentDatas.Add(paymentLastRow);
                        Console.WriteLine(nameof(PrepareData) + " | " + (period) + " | " + contractNo);
                        continue;
                    }

                    if (period == eirPaymentSchedule.PeriodNo)
                    {
                        PaymentEffectiveRateLog paymentLastRow = new PaymentEffectiveRateLog();
                        paymentLastRow.ContractNo = contractNo;
                        paymentLastRow.SlipNo = string.Empty;
                        paymentLastRow.SlipDate = lastPayment.SlipDate;
                        paymentLastRow.LastCalculateInterestDate = lastPayment.SlipDate;
                        paymentLastRow.Period = eirPaymentSchedule.PeriodNo;
                        paymentLastRow.ContractDueDate = eirPaymentSchedule.DueDate;
                        paymentLastRow.EffectiveDay = 0;
                        paymentLastRow.InstallmentAmnt = installmentAmnt;
                        paymentLastRow.NetAmnt = 0;
                        paymentLastRow.PayInstallmentAmnt = 0;
                        paymentLastRow.OverPayInstallmentAmnt = 0;
                        paymentLastRow.CalculateInterestAmnt = 0;
                        paymentLastRow.PayInterestAmnt = 0;
                        paymentLastRow.PayOldInterestAmnt = 0;
                        paymentLastRow.PayNewInterestAmnt = 0;
                        paymentLastRow.PayPrincipalAmnt = 0;
                        paymentLastRow.RemainPrincipalAmnt = lastPayment.RemainPrincipalAmnt;
                        paymentLastRow.UnpaidInterestAmnt = lastPayment.UnpaidInterestAmnt;
                        paymentLastRow.RemainInstallmentAmnt = paymentLastRow.InstallmentAmnt - eirPaymentSchedule.PaymentAmount;
                        paymentLastRow.IsComplete = false;
                        paymentLastRow.IsCancel = false;
                        paymentLastRow.Remark = string.Empty;

                        paymentDatas.Add(paymentLastRow);
                        Console.WriteLine(nameof(PrepareData) + " | " + (period) + " | " + contractNo);
                    }

                    bool isGenfuture = period >= eirPaymentSchedule.PeriodNo;
                    if (isGenfuture)
                    {
                        PaymentEffectiveRateLog row = paymentDatas.Find(a => a.Period == period);
                        if (period != eirPaymentSchedule.PeriodNo)
                            row = paymentDatas.Find(a => a.Period == (period - 1) && a.IsComplete);

                        List<PaymentEffectiveRateLog> paymentCalculates = GetCalculates(contractNo, contractData, contractTDR, period, row, period == eirPaymentSchedule.PeriodNo);
                        paymentDatas.AddRange(paymentCalculates);
                        Console.WriteLine(nameof(PrepareData) + " | " + (period) + " | " + contractNo);
                        continue;
                    }
                }
            }
        }

        public void Save()
        {
            if (!paymentDatas.Any())
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine("------Data does is not change !!------");
                Console.WriteLine("------Data don't macthing genaratec condition !!------");
                Console.ResetColor();
                return;
            }

            SaveProcess();
        }

        private void SaveProcess()
        {
            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            conn.Open();
            MySqlTransaction tran = conn.BeginTransaction();

            Console.ForegroundColor = ConsoleColor.Blue;
            try
            {
                int loop = 0;
                string query = string.Empty;
                foreach (PaymentEffectiveRateLog data in paymentDatas)
                {
                    query += QuerySaveData(data);
                    Console.WriteLine(nameof(SaveProcess) + " | " + (++loop) + " | " + data.ContractNo);

                    if (loop % 500 == 0 || loop == paymentDatas.Count)
                    {
                        if (string.IsNullOrEmpty(query))
                            continue;

                        MySqlCommand cmd = conn.CreateCommand();
                        cmd.Transaction = tran;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                        Console.WriteLine("ExecuteNonQuery");
                        query = string.Empty;
                    }
                }

                tran.Commit();

                Console.WriteLine();
                Console.WriteLine("------ Save Process Success !!------");
                Console.ResetColor();
            }
            catch (Exception ex)
            {

                tran.Rollback();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine("------ Error Save Process !!------");
                Console.ResetColor();
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();

                tran.Dispose();
            }

        }
        #endregion

        #region Private Method
        public bool IsAll()
        {
            return contractNO.ToLower() == "all";
        }

        private List<ContractData> GetContractDatas()
        {
            List<ContractData> contractDatas = new List<ContractData>();
            string query = "SELECT ls.SY_NO , ls.SY_DATE , ls.SY_PAY_DAY , ls.SY_START_DATE , ls.SY_MTH_CST , ls.SY_MTH_AMT , ls.SY_COST ,ls.SY_PK_AMT , ls.EIR_RATE , ls.SY_MONTH , ls.CLOSE_DATE "
                            + "FROM leasing.leasing ls "
                            + "WHERE ls.SY_NO <> ''  "
                            + "AND(ls.SY_NO LIKE '%PL%' OR ls.SY_NO LIKE '%NF%') GROUP BY ls.SY_NO";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ContractData data = new ContractData
                    {
                        ContractNo = ReaderHepler.GetValueFromReader<string>(reader, "SY_NO"),
                        ContractDate = ReaderHepler.GetValueFromReader<DateTime>(reader, "SY_DATE"),
                        PayDay = ReaderHepler.GetValueFromReader<string>(reader, "SY_PAY_DAY"),
                        FirstPeriodDate = ReaderHepler.GetValueFromReader<DateTime>(reader, "SY_START_DATE"),
                        SyMthAmt = ReaderHepler.GetValueFromReader<decimal>(reader, "SY_MTH_AMT"),
                        SyCost = ReaderHepler.GetValueFromReader<decimal>(reader, "SY_COST"),
                        SyPkAmt = ReaderHepler.GetValueFromReader<decimal>(reader, "SY_PK_AMT"),
                        EirRate = ReaderHepler.GetValueFromReader<decimal>(reader, "EIR_RATE"),
                        SyMonth = ReaderHepler.GetValueFromReader<int>(reader, "SY_MONTH"),
                        CloseDate = ReaderHepler.GetValueFromReader<DateTime?>(reader, "CLOSE_DATE"),
                    };

                    contractDatas.Add(data);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return contractDatas;
        }

        private List<string> GetAllContractNoPaymentEffectiveLog()
        {
            List<string> contractNos = new List<string>();
            string query = "SELECT distinct(p.ContractNo) AS ContractNo  FROM leasing.paymenteffectiveratelog p WHERE p.IsCancel = 0 ;";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    contractNos.Add(reader["ContractNo"].ToString());
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return contractNos;
        }

        private PaymentEffectiveRateLog GetLastPayment(string contractNo)
        {
            string query = " SELECT *  FROM leasing.paymenteffectiveratelog p "
                         + " WHERE p.ContractNo = '" + contractNo + "' "
                         + " AND p.IsCancel = 0 "
                         + " ORDER BY p.SlipDate DESC "
                         + " LIMIT 1; ";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return new PaymentEffectiveRateLog
                    {
                        PaymentEffectiveRateLogID = ReaderHepler.GetValueFromReader<int>(reader, nameof(PaymentEffectiveRateLog.PaymentEffectiveRateLogID)),
                        ContractNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.ContractNo)),
                        SlipNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.SlipNo)),
                        SlipDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(PaymentEffectiveRateLog.SlipDate)),
                        EffectiveDay = ReaderHepler.GetValueFromReader<int>(reader, nameof(PaymentEffectiveRateLog.EffectiveDay)),
                        CalculateInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.CalculateInterestAmnt)),
                        BringForwardPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.BringForwardPrincipalAmnt)),
                        BringForwardInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.BringForwardInterestAmnt)),
                        PayInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayInterestAmnt)),
                        PayOldInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayOldInterestAmnt)),
                        PayNewInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayNewInterestAmnt)),
                        PayPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayPrincipalAmnt)),
                        RemainPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.RemainPrincipalAmnt)),
                        UnpaidInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.UnpaidInterestAmnt)),
                        NetAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.NetAmnt)),
                        IsCancel = ReaderHepler.GetValueFromReader<bool>(reader, nameof(PaymentEffectiveRateLog.IsCancel)),
                        Remark = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.Remark)),
                        CreatedBy = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.CreatedBy)),
                        CreatedDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(PaymentEffectiveRateLog.CreatedDate)),
                    };

                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return null;
        }

        private List<PaymentEffectiveRateLog> GetLastPaymentAllContract()
        {
            List<PaymentEffectiveRateLog> datas = new List<PaymentEffectiveRateLog>();
            string query = " SELECT * "
                        + "FROM leasing.paymenteffectiveratelog p "
                        + "INNER JOIN ( "
                        + "SELECT MAX(p.SlipDate) AS SlipDate, MAX(p.PaymentEffectiveRateLogID) AS PaymentEffectiveRateLogID, p.ContractNo "
                        + "FROM leasing.paymenteffectiveratelog p "
                        + "WHERE p.IsCancel = 0 "
                        + "GROUP BY p.ContractNo "
                        + ") AS pe ON pe.SlipDate = p.SlipDate AND p.PaymentEffectiveRateLogID = pe.PaymentEffectiveRateLogID AND pe.ContractNo = p.ContractNo "
                        + "; ";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    datas.Add(new PaymentEffectiveRateLog
                    {
                        PaymentEffectiveRateLogID = ReaderHepler.GetValueFromReader<int>(reader, nameof(PaymentEffectiveRateLog.PaymentEffectiveRateLogID)),
                        ContractNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.ContractNo)),
                        SlipNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.SlipNo)),
                        SlipDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(PaymentEffectiveRateLog.SlipDate)),
                        EffectiveDay = ReaderHepler.GetValueFromReader<int>(reader, nameof(PaymentEffectiveRateLog.EffectiveDay)),
                        CalculateInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.CalculateInterestAmnt)),
                        BringForwardPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.BringForwardPrincipalAmnt)),
                        BringForwardInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.BringForwardInterestAmnt)),
                        PayInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayInterestAmnt)),
                        PayOldInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayOldInterestAmnt)),
                        PayNewInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayNewInterestAmnt)),
                        PayPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.PayPrincipalAmnt)),
                        RemainPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.RemainPrincipalAmnt)),
                        UnpaidInterestAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.UnpaidInterestAmnt)),
                        NetAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(PaymentEffectiveRateLog.NetAmnt)),
                        IsCancel = ReaderHepler.GetValueFromReader<bool>(reader, nameof(PaymentEffectiveRateLog.IsCancel)),
                        Remark = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.Remark)),
                        CreatedBy = ReaderHepler.GetValueFromReader<string>(reader, nameof(PaymentEffectiveRateLog.CreatedBy)),
                        CreatedDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(PaymentEffectiveRateLog.CreatedDate)),
                    });

                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return datas;
        }

        private List<EirPaymentSchedule> GetAllPaymentSchedule()
        {
            List<EirPaymentSchedule> datas = new List<EirPaymentSchedule>();
            string query = "SELECT e.* "
                        + " FROM leasing.eirpaymentschedule e "
                        + " INNER JOIN leasing.leasing ls ON ls.SY_NO = e.ContractNo "
                        + " WHERE ls.SY_NO <> '' AND ( ls.SY_NO LIKE '%PL%' OR ls.SY_NO LIKE '%NF%' ) "
                        + " AND e.PaymentAmount > 0 ";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    datas.Add(new EirPaymentSchedule
                    {
                        ContractNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(EirPaymentSchedule.ContractNo)),
                        PeriodNo = ReaderHepler.GetValueFromReader<int>(reader, nameof(EirPaymentSchedule.PeriodNo)),
                        DueDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(EirPaymentSchedule.DueDate)),
                        InstallmentAmount = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(EirPaymentSchedule.InstallmentAmount)),
                        PaymentAmount = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(EirPaymentSchedule.PaymentAmount)),
                        BalanceAmount = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(EirPaymentSchedule.BalanceAmount)),
                        SlipDate = ReaderHepler.GetValueFromReader<DateTime?>(reader, nameof(EirPaymentSchedule.SlipDate)),
                        CreateDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(EirPaymentSchedule.CreateDate)),
                    });
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return datas;
        }

        private List<EirPaymentSchedule> GetPeriodNonPayments()
        {
            List<EirPaymentSchedule> datas = new List<EirPaymentSchedule>();
            string query = "SELECT * "
                        + " FROM leasing.eirpaymentschedule e "
                        + " INNER JOIN ( "
                        + " SELECT MIN(e.DueDate) AS DueDate, e.ContractNo "
                        + " FROM leasing.eirpaymentschedule e "
                        + " WHERE e.SlipDate IS NULL "
                        + " GROUP BY e.ContractNo "
                        + " ) AS er ON er.DueDate = e.DueDate AND er.ContractNo = e.ContractNo";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    datas.Add(new EirPaymentSchedule
                    {
                        ContractNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(EirPaymentSchedule.ContractNo)),
                        PeriodNo = ReaderHepler.GetValueFromReader<int>(reader, nameof(EirPaymentSchedule.PeriodNo)),
                        DueDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(EirPaymentSchedule.DueDate)),
                        InstallmentAmount = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(EirPaymentSchedule.InstallmentAmount)),
                        PaymentAmount = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(EirPaymentSchedule.PaymentAmount)),
                        BalanceAmount = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(EirPaymentSchedule.BalanceAmount)),
                        SlipDate = ReaderHepler.GetValueFromReader<DateTime?>(reader, nameof(EirPaymentSchedule.SlipDate)),
                        CreateDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(EirPaymentSchedule.CreateDate)),
                    });
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return datas;
        }

        private List<ContractTDR> GetDataContractTDR()
        {
            List<ContractTDR> datas = new List<ContractTDR>();
            string query = "SELECT * FROM leasing.contract_tdr t "
                            + " INNER JOIN leasing.contract_tdr_payment_term tt ON t.ContractTDRID = tt.ContractTDRID "
                            + " WHERE t.ApprovedBy <> '' AND t.ApprovedDate IS NOT NULL "
                            + " AND t.CanceledBy = '' AND t.CanceledDate IS NULL ";

            MySqlConnection conn = new MySqlConnection(GlobalVar.ServerConectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    datas.Add(new ContractTDR
                    {
                        ContractTDRID = ReaderHepler.GetValueFromReader<int>(reader, nameof(ContractTDR.ContractTDRID)),
                        ContractNo = ReaderHepler.GetValueFromReader<string>(reader, nameof(ContractTDR.ContractNo)),
                        RemainPrincipalAmnt = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(ContractTDR.RemainPrincipalAmnt)),
                        ApprovedDate = ReaderHepler.GetValueFromReader<DateTime?>(reader, nameof(ContractTDR.ApprovedDate)),
                        ApprovedBy = ReaderHepler.GetValueFromReader<string>(reader, nameof(ContractTDR.ApprovedBy)),
                        ContractTDRPaymentTermID = ReaderHepler.GetValueFromReader<int>(reader, nameof(ContractTDR.ContractTDRPaymentTermID)),
                        PeroidDate = ReaderHepler.GetValueFromReader<DateTime>(reader, nameof(ContractTDR.PeroidDate)),
                        PeroidNo = ReaderHepler.GetValueFromReader<int>(reader, nameof(ContractTDR.PeroidNo)),
                        Installment = ReaderHepler.GetValueFromReader<decimal>(reader, nameof(ContractTDR.Installment)),
                    });
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return datas;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return datas;
        }

        private string QuerySaveData(PaymentEffectiveRateLog data)
        {
            List<QuerySource> querySources = new List<QuerySource>();

            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.ContractNo), Value = StringFormatHelper.GetSingleQuote(data.ContractNo) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.SlipNo), Value = StringFormatHelper.GetSingleQuote(data.SlipNo) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.SlipDate), Value = StringFormatHelper.GetSingleQuote(StringFormatHelper.GetShortEnDateFormat(data.SlipDate)) });

            if (data.LastCalculateInterestDate.HasValue)
                querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.LastCalculateInterestDate), Value = StringFormatHelper.GetSingleQuote(StringFormatHelper.GetShortEnDateFormat(data.LastCalculateInterestDate.Value)) });

            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.Period), Value = (data.Period) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.ContractDueDate), Value = StringFormatHelper.GetSingleQuote(StringFormatHelper.GetShortEnDateFormat(data.ContractDueDate.Value)) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.EffectiveDay), Value = (data.EffectiveDay) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.InstallmentAmnt), Value = (data.InstallmentAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.PayInstallmentAmnt), Value = (data.PayInstallmentAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.OverPayInstallmentAmnt), Value = (data.OverPayInstallmentAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.CalculateInterestAmnt), Value = (data.CalculateInterestAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.BringForwardPrincipalAmnt), Value = (data.BringForwardPrincipalAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.BringForwardInterestAmnt), Value = (data.BringForwardInterestAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.PayInterestAmnt), Value = (data.PayInterestAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.PayOldInterestAmnt), Value = (data.PayOldInterestAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.PayNewInterestAmnt), Value = (data.PayNewInterestAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.PayPrincipalAmnt), Value = (data.PayPrincipalAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.RemainPrincipalAmnt), Value = (data.RemainPrincipalAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.UnpaidInterestAmnt), Value = (data.UnpaidInterestAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.NetAmnt), Value = (data.NetAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.RemainInstallmentAmnt), Value = (data.RemainInstallmentAmnt) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.IsComplete), Value = (Convert.ToInt32(data.IsComplete)) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.IsCancel), Value = (Convert.ToInt32(data.IsCancel)) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.Remark), Value = StringFormatHelper.GetSingleQuote(data.Remark) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.CreatedBy), Value = StringFormatHelper.GetSingleQuote(GlobalVar.UserID) });
            querySources.Add(new QuerySource { FieldName = nameof(PaymentEffectiveRateLog.CreatedDate), Value = StringFormatHelper.GetSingleQuote(StringFormatHelper.GetShortEnDateAndTimeFormat(DateTime.Now)) });

            var fieldNames = querySources.Select(a => a.FieldName).ToArray();
            var values = querySources.Select(a => a.Value).ToArray();

            string query = "INSERT INTO leasing.paymenteffectiveratelog ( " + string.Join(",", fieldNames)
                    + ") VALUES ( " + string.Join(",", values) + " "
                    + "); " + Environment.NewLine
                    ;

            return query;
        }

        #endregion

        #region
        private List<PaymentEffectiveRateLog> GetCalculates(string contractNo, ContractData leasing, ContractTDR contractTDR, int period, PaymentEffectiveRateLog lastPayment = null, bool isLastSlipPeriod = false)
        {
            List<PaymentEffectiveRateLog> calculateResults = new List<PaymentEffectiveRateLog>();

            PaymentEffectiveRateLog paymentEffectiveRateLog = lastPayment;

            decimal payAmount = 0;
            DateTime payDate = new DateTime();
            if (period == 1)
            {
                paymentEffectiveRateLog = GetDefaultLastPayment(leasing);
                payAmount = paymentEffectiveRateLog.InstallmentAmnt;
                payDate = paymentEffectiveRateLog.ContractDueDate.Value;
            }
            else if (!isLastSlipPeriod)
            {
                payDate = paymentEffectiveRateLog.ContractDueDate.Value.AddMonths(1);
                payAmount = FindInstallemntAmnt(leasing, period, payDate, contractTDR);
            }
            else
            {
                payAmount = paymentEffectiveRateLog.RemainInstallmentAmnt;
                payDate = paymentEffectiveRateLog.ContractDueDate.Value;
            }

            CalculatePayment(contractNo, payAmount, payDate, paymentEffectiveRateLog, calculateResults, leasing, contractTDR);

            //foreach (PaymentEffectiveRateLog data in calculateResults)
            //{

            //}

            return calculateResults;
        }

        public PaymentEffectiveRateLog GetDefaultLastPayment(ContractData leasing)
        {
            return new PaymentEffectiveRateLog
            {
                SlipNo = string.Empty,
                SlipDate = leasing.ContractDate,
                EffectiveDay = 0,
                CalculateInterestAmnt = 0,
                BringForwardPrincipalAmnt = 0,
                BringForwardInterestAmnt = 0,
                PayInterestAmnt = 0,
                PayOldInterestAmnt = 0,
                PayNewInterestAmnt = 0,
                PayPrincipalAmnt = 0,
                RemainPrincipalAmnt = leasing.PrincipalAmnt,
                UnpaidInterestAmnt = 0,
                NetAmnt = 0,
                LastCalculateInterestDate = leasing.ContractDate,
                Period = 0,
                ContractDueDate = leasing.FirstPeriodDate.AddMonths(-1),
                InstallmentAmnt = 0,
                PayInstallmentAmnt = 0,
                OverPayInstallmentAmnt = 0,
                RemainInstallmentAmnt = 0,
                IsComplete = true,
            };
        }

        public void CalculatePayment(string contractNo, decimal payAmount, DateTime payDate, PaymentEffectiveRateLog lastPayment, List<PaymentEffectiveRateLog> calculateResults, ContractData leasing, ContractTDR contractTDR)
        {
            if (lastPayment is null)
                throw new Exception("Contract is not found.");

            string slipNo = "SlipNoNew";
            int period = lastPayment.Period;
            DateTime dueDate = lastPayment.ContractDueDate.Value.Date;
            decimal installmentAmnt = lastPayment.InstallmentAmnt;

            if (lastPayment.IsComplete && (payDate.Date > dueDate.Date) || period == 0) // จ่ายครบงวงด และ จ่ายหลัง Due หรือ สัญญาใหม่
            {
                ++period;
                dueDate = dueDate.AddMonths(1);
                installmentAmnt = FindInstallemntAmnt(leasing, period, dueDate, contractTDR);
            }
            bool isSamePeriod = (period == lastPayment.Period); // งวดเดิม

            DateTime lastCalculateInterestDate = lastPayment.LastCalculateInterestDate.Value;
            DateTime calInterestDate = new DateTime();
            if (lastPayment.LastCalculateInterestDate.Value.Date > dueDate) // คิดดอกเบี้ย > Due
                calInterestDate = lastPayment.LastCalculateInterestDate.Value;
            else if (payDate < dueDate) // จ่ายหลัง due ใช้ due  ก่อน Due ใช้วันที่ชำระ
                calInterestDate = payDate;
            else
                calInterestDate = dueDate;

            bool isDelayedPay = payDate > dueDate; // จ่ายล่าช้า

            decimal bringForwardPrincipalAmnt = GetBringForwardPrincipalAmnt(lastPayment, leasing);
            decimal bringForwardInterestAmnt = GetBringForwardInterestAmnt(lastPayment);
            int effectiveDay = GetCalculationDays(calInterestDate, lastCalculateInterestDate);
            decimal calculateInterestAmnt = GetCalculateInterestAmnt(leasing.EirRate, bringForwardPrincipalAmnt, effectiveDay);
            decimal unpaidInterestAmnt = 0;
            decimal payOldInterestAmnt = 0;
            decimal payNewInterestAmnt = 0;
            decimal payPrincipalAmnt = 0;
            decimal remainPrincipalAmnt = 0;
            decimal netAmnt = Math.Round(payAmount, 2);

            // หาชำระค่างวด
            decimal payInstallmentAmnt = 0;
            bool isSameSlip = slipNo == lastPayment.SlipNo;
            if (isSamePeriod && isDelayedPay) // งวดเดิม และ จ่ายงวด
            {
                decimal amnt = !isSameSlip ? netAmnt : netAmnt - lastPayment.PayInstallmentAmnt;
                if (amnt < lastPayment.RemainInstallmentAmnt)
                    payInstallmentAmnt = amnt;
                else
                    payInstallmentAmnt = lastPayment.RemainInstallmentAmnt;
            }
            else if (isSameSlip && lastPayment.OverPayInstallmentAmnt > installmentAmnt && isDelayedPay)
                payInstallmentAmnt = installmentAmnt;

            else if (payDate >= dueDate)
            {
                payInstallmentAmnt = isSameSlip ? lastPayment.OverPayInstallmentAmnt : netAmnt;
            }
            else if (!isDelayedPay)
            {
                payInstallmentAmnt = isSameSlip ? lastPayment.OverPayInstallmentAmnt : netAmnt;
            }
            else if (isSameSlip)
            {
                payInstallmentAmnt = lastPayment.OverPayInstallmentAmnt;
            }
            else
            {
                payInstallmentAmnt = (netAmnt < installmentAmnt) ? netAmnt : installmentAmnt;
            }

            //IF(C8 >= F8, IF(B8 = B7, K7, I8), IF(C8 <= F8, IF(B8 = B7, K7, I8), IF(B8 = B7, K7, IF(I8 < H8, I8, H8))))

            // IF(AND(E8 = E7, C8 > F8), IF(IF(B8 <> B7, I8, I8 - J7) < S7, IF(B8 <> B7, I8, I8 - J7), S7)
            //, IF(AND(B8 = B7, K7 > H8, C8 > F8), H8, IF(C8 >= F8, IF(B8 = B7, K7, I8), IF(C8 <= F8, IF(B8 = B7, K7, I8), IF(B8 = B7, K7, IF(I8 < H8, I8, H8))))))

            decimal overPayInstallmentAmnt = 0; // หายอดชำระเกิน
            if (isSameSlip) // ใบเสร็จเดียวกัน = ยอดชำระเกิน - ยอดชำระค่างวด ! ยอดชำะเกิน + ยอดชำระ - ยอดชำระค่างวด
                overPayInstallmentAmnt = lastPayment.OverPayInstallmentAmnt - payInstallmentAmnt;
            else
                overPayInstallmentAmnt = lastPayment.OverPayInstallmentAmnt + netAmnt - payInstallmentAmnt; // ค่างวดชำระเกิน = ค่างวดชำระเกิดงวงดแล้ว + จำนวนเงิน - ชำระค่างวด

            if (payInstallmentAmnt > bringForwardInterestAmnt) // 
                payOldInterestAmnt = bringForwardInterestAmnt;
            else
            {
                payOldInterestAmnt = payInstallmentAmnt;
                unpaidInterestAmnt = unpaidInterestAmnt + bringForwardInterestAmnt - payInstallmentAmnt;
            }

            decimal payAmountCurrent = payInstallmentAmnt - payOldInterestAmnt;

            if (payAmountCurrent > calculateInterestAmnt)
            {
                payNewInterestAmnt = calculateInterestAmnt;
            }
            else
            {
                payNewInterestAmnt = payAmountCurrent;
                unpaidInterestAmnt = unpaidInterestAmnt + calculateInterestAmnt - payAmountCurrent;
            }
            payAmountCurrent = payAmountCurrent - payNewInterestAmnt;

            payPrincipalAmnt = payAmountCurrent;
            remainPrincipalAmnt = bringForwardPrincipalAmnt - payPrincipalAmnt;

            decimal remainInstallmentAmnt = 0; // ค่างวดคงเหลือ
            if (isSamePeriod)
                remainInstallmentAmnt = lastPayment.RemainInstallmentAmnt - payInstallmentAmnt;
            else if (installmentAmnt - payInstallmentAmnt > 0)
                remainInstallmentAmnt = installmentAmnt - payInstallmentAmnt;

            if (remainInstallmentAmnt < 0)
                remainInstallmentAmnt = 0;

            bool isComplete = false;
            if (remainInstallmentAmnt == 0)
                isComplete = true;

            calculateResults.Add(new PaymentEffectiveRateLog()
            {
                ContractNo = contractNo,
                EffectiveDay = effectiveDay,
                CalculateInterestAmnt = Math.Round(calculateInterestAmnt, 2),
                BringForwardPrincipalAmnt = Math.Round(bringForwardPrincipalAmnt, 2),
                BringForwardInterestAmnt = Math.Round(bringForwardInterestAmnt, 2),
                PayInterestAmnt = Math.Round(payOldInterestAmnt + payNewInterestAmnt, 2),
                PayOldInterestAmnt = Math.Round(payOldInterestAmnt, 2),
                PayNewInterestAmnt = Math.Round(payNewInterestAmnt, 2),
                PayPrincipalAmnt = Math.Round(payPrincipalAmnt, 2),
                RemainPrincipalAmnt = Math.Round(remainPrincipalAmnt, 2),
                UnpaidInterestAmnt = Math.Round(unpaidInterestAmnt, 2),
                NetAmnt = netAmnt,
                LastCalculateInterestDate = calInterestDate,
                Period = period,
                ContractDueDate = dueDate,
                InstallmentAmnt = installmentAmnt,
                PayInstallmentAmnt = payInstallmentAmnt,
                OverPayInstallmentAmnt = overPayInstallmentAmnt,
                RemainInstallmentAmnt = remainInstallmentAmnt,
                IsComplete = isComplete,
                IsCancel = false,
                SlipDate = payDate,
                PaymentEffectiveRateLogID = 0,
                SlipNo = string.Empty,
                Remark = string.Empty,
            });

            // ตรวจเช็คว่าพอชำระงวดถัดไปหรือไม่
            if (!isComplete || !(payDate.Date > dueDate.Date))
                return;

            //++period;
            //dueDate = dueDate.AddMonths(1);
            //decimal installmentAmntNext = FindInstallemntAmnt(leasing, period, dueDate, contractTDR);

            if (overPayInstallmentAmnt <= 0)
                return;

            PaymentEffectiveRateLog calculateResult = calculateResults.LastOrDefault();

            PaymentEffectiveRateLog lastPaymentNext = new PaymentEffectiveRateLog
            {
                SlipNo = slipNo,
                EffectiveDay = calculateResult.EffectiveDay,
                CalculateInterestAmnt = calculateResult.CalculateInterestAmnt,
                BringForwardPrincipalAmnt = calculateResult.BringForwardPrincipalAmnt,
                BringForwardInterestAmnt = calculateResult.BringForwardInterestAmnt,
                PayInterestAmnt = calculateResult.PayInterestAmnt,
                PayOldInterestAmnt = calculateResult.PayOldInterestAmnt,
                PayNewInterestAmnt = calculateResult.PayNewInterestAmnt,
                PayPrincipalAmnt = calculateResult.PayPrincipalAmnt,
                RemainPrincipalAmnt = calculateResult.RemainPrincipalAmnt,
                UnpaidInterestAmnt = calculateResult.UnpaidInterestAmnt,
                NetAmnt = calculateResult.NetAmnt,
                LastCalculateInterestDate = calculateResult.LastCalculateInterestDate,
                Period = calculateResult.Period,
                ContractDueDate = calculateResult.ContractDueDate,
                InstallmentAmnt = calculateResult.InstallmentAmnt,
                PayInstallmentAmnt = calculateResult.PayInstallmentAmnt,
                OverPayInstallmentAmnt = calculateResult.OverPayInstallmentAmnt,
                RemainInstallmentAmnt = calculateResult.RemainInstallmentAmnt,
                IsComplete = calculateResult.IsComplete,
            };

            CalculatePayment(contractNo, netAmnt, payDate, lastPaymentNext, calculateResults, leasing, contractTDR);
        }

        private decimal FindInstallemntAmnt(ContractData leasing, int period, DateTime dueDate, ContractTDR contractTDR)
        {
            if (contractTDR is null)
                return leasing.SyMthAmt;

            return contractTDR.Installment;
        }

        private decimal GetBringForwardInterestAmnt(PaymentEffectiveRateLog lastPaymentEffectiveRateLog)
        {
            if (lastPaymentEffectiveRateLog == null)
                return 0;

            return lastPaymentEffectiveRateLog.UnpaidInterestAmnt;
        }

        private decimal GetBringForwardPrincipalAmnt(PaymentEffectiveRateLog lastPaymentEffectiveRateLog, ContractData leasing)
        {
            if (lastPaymentEffectiveRateLog == null)
                return leasing.PrincipalAmnt;

            return lastPaymentEffectiveRateLog.RemainPrincipalAmnt;
        }

        private decimal GetCalculateInterestAmnt(decimal eirRate, decimal bringForwardPrincipalAmnt, int effectiveDay)
        {
            return effectiveDay * eirRate * bringForwardPrincipalAmnt;
        }

        private int GetCalculationDays(DateTime payDate, DateTime lastPayDate)
        {
            return (int)(payDate.Date - lastPayDate.Date).TotalDays;
        }
        #endregion
    }
}
